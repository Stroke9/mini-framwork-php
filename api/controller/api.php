<?php
/**
 * Dummy API, trying to reproduce the 'lePetitCoin' API
 * All files in /api are for testing purposes only !
 * Please, take consideration of making comment and / or edit this files to improve the project !
 */


// Class with all 'routes'
include dirname(__DIR__).'/model/routing/Routing.class.php';


// First take the 'action' <=> to the 'method' called in classing Routing systems
if(isset($_GET['action'])){
    $action=$_GET['action'];
}else{
    // Default action, to make the API make at least a response
    $action='test';
}

if(isset($_GET['option'])){
    $option=$_GET['option'];
}else{
    // Default option, to make the API make at least a response
    $option='';
}

// Then bind a param to it ; for a classic routing, it would be the parameters called after the method, separated with '/' (slashes)
if(isset($_POST) && !empty($_POST)){
    $value=$_POST;
}elseif(isset($_GET['value'])){
    $value=$_GET['value'];
}else{
    $value='test';
}

// Instaciate our 'routeur', that will called the proper method, and return a json that'll be send to our 'client' with the echo function
$routeur = new Route($action,$value,$option);
echo $routeur->result;
