<?php

include 'BDD.class.php';
/**
 * MyEntity represent the table in our Database to CRUD ON
 * For example, if you want to access the table 'mytable', just specify it when you first instanciate this object
 * This Class use method from BDD and others to access the Database, to build the request AND parse the values
 */
class MyEntity{

    public $bdd;
    public $message;

    private $table;
    private $columnName;

    /** Here we needed to build a BDD object, so we need to pass the dbname here plus the name of the entity */
    
    public function __construct($dbname, $tablename){
        $this->message=['danger'=>[],'sucess'=>[]];
        $this->bdd=new BDD($dbname);
        $this->table = $tablename;
        $this->message = ["danger" => [], "success" => []];
    }

    /**
     * /!\ NEED to add the options management (cf. buildRequestOption method)
     */
    public function post($column = '', $options){ 
        // Prepare the target of our query
        $this->selectColumn($column);

        // Prepare the query
        $sql = "INSERT INTO $this->table $this->columnName VALUES ($options)";

        // Execution
        $req = $this->bdd->callMethod('prepare', $sql);
        $req->execute();

        // Error and Log Management
        if(!$req){
            $this->message['sucess'][] = 'Add Successfully !';
        }else{
            $this->message['danger'][] = 'Error Occured, while posting data';
        }
        
    }

    /**
     * In developpement !
     */
    public function update($optionSet = '', $optionWhere){
        $sql = "UPDATE " . $this->table . " SET " . $optionSet . $optionWhere;
        $req = $this->bdd->callMethod('prepare', $sql);
        $sql->execute();
        array_push($this->message["success"], "Update Successfully !");
    }
    
    public function get($column, $option='', $methodOut = "fetch"){
        // Prepare the target of our query
        $columnName=$this->selectColumn($column);

        // Prepare the query
        $sql = "SELECT $columnName FROM $this->table $option";
        
        // Execute
        $req = $this->bdd->callMethod('prepare', $sql);
        $req->execute();
        
        // Output
 
        $result = $req->$methodOut(PDO::FETCH_ASSOC); // Format the result for a better output
        array_push($this->message["success"], "Get Successfully !");
        return $result;

    }

    /**
     * In developpement !
     */
    public function getJoin($column, $tableJoin, $option = '') {
        $this->selectColomn($column);
    }

    /**
     * To test / To implement
     */
    public function delete($option = ''){  
        $sql = "DELETE FROM " . $this->table . $option;
        $sql->execute();
        $this->bdd = null;
        array_push($this->message["success"], "Delete Successfully !");
        
    }

    /**
     * To test / To implement
     */
    public function buildRequestOption(...$options){
        foreach($options as $option){
            $result = $option;
        }
        return $result;
    }

    public function selectColumn($columns = '*') {
        if(gettype($columns)== 'array'){
            foreach($columns as $column){
                $columnName.='`'.$column.'`,';
            }
            $columnName=preg_replace('/(.*),/','$1 and',$columnName); // Other regex : (,)[^,]*$
        }else{
            $columnName=$columns;
        }
        return $columnName;
    }

    public function __toString(){
        $bddLog = $this->bdd->getLog();
        $entityLog = $this->message;
        $log = ['bdd'=>$bddLog,'messages'=>$entityLog];  
        return $log;
    }

}

/**
 * FOR DEVELOPPEMENT PURPOSES ONLY !
 */


/* 
$entity = new MyEntity('leptitcoin', 'category');
}  

/* $entity = new MyEntity('lepticoin', 'category');
$var = $entity->post("(`name`)", "'Ma Cat'");
var_dump($var);
echo $entity->message;
echo $entity->bdd;

/*$entity->get("*",$option);
$entity = new MyEntity('leptitcoin', 'account');
$entity->get("*", "", "fetchAll"); */
/* 
$entity->get("*",$option);
$entity = new MyEntity('lepticoin', 'account');
$entity->get("*", "", "fetchAll");

$option=buildRequestOption(['where'=>['column_name','conditionnal_operator','value']]);
getMyEntity(array('id','name','fname'));

/*$entity->get("*",$option);
$entity = new MyEntity('leptitcoin', 'account');
$entity->get("*", "", "fetchAll"); */
/* 
$entity->get("*",$option);
$entity = new MyEntity('lepticoin', 'account');
$entity->get("*", "", "fetchAll");

$option=buildRequestOption(['where'=>['column_name','conditionnal_operator','value']]);
getMyEntity(array('id','name','fname')); */
