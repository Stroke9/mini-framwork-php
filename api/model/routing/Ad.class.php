<?php 

    class Ad{

        public function ads($entity){
            $data = $entity->get("*", "", "fetchAll");
            $message = $entity->message;
            $dataJson = ['data' => $data, 'message' => $message];
            $json = json_encode($dataJson);
            return $json;
        }

        public function adCreation($entity,$value){
            //session_start();

            $this->customer=$_SESSION['id'];
            $this->category = $value['category'];
            $this->title = $value['title'];
            $this->description = $value['text'];
            $this->price = $value['price'];
            $this->zip_code = $value['zip_code'];
            $this->date = date("Y-m-d");

            return $entity->post("id_customer_id,id_category_id,title,date,description,zip_code,price","'1','$this->category','$this->title','$this->date','$this->description','$this->zip_code','$this->price'");
        }

        /**
         * Need to BIND with Routing.class.php  
         */ 
        public function adValider($entity,$value='', $option="="){

            $this->name = $value;
            $data = $entity->get("*", "WHERE status = 'success'", "fetchAll");
            $message = $entity->message;
            $dataJson = ['data' => $data, 'message' => $message];
            $json = json_encode($dataJson);
            return $json;
        }

        /**
         * Need to BIND with Routing.class.php  
         */ 
        public function adSearch($entity, $value='', $option = false){
            if (!$option) {
                $data = $entity->get("*", "WHERE CONCAT(title, description) LIKE '%$value%'", "fetchAll");
            } else {
                $optionData = json_decode($option);
                foreach($optionData as $key => $val) {
                    $data = $entity->get("*", "WHERE $key LIKE '%$val%'", "fetchAll");
                }
            }
            $message = $entity->message;
            $dataJson = ['data' => $data, 'message' => $message];
            $json = json_encode($dataJson);
            return $json;
        }

        
        public function adPending($value=''){

            $this->name = $value;
            $this->entity = new MyEntity('leptitcoin', 'ad');
            $data = $this->entity->get("*", "WHERE status = 'pending'", "fetchAll");
            $message = $this->entity->message;
            $dataJson = ['data' => $data, 'message' => $message];
            $json = json_encode($dataJson);
            return $json;
            
        }

        public function deleteAd($id){
            
            var_dump($id);
            $id = gettype($id) == 'array' ? json_decode($id)['value'] : $id;
            return "Entry with id '$id' was succesfuly deleted !";
        }

            /**
         * @Route("/adUpdate/", methods={'POST'}, name="adUpdate")
         */
        public function updateAd($value){

            //$idAd=$value['id'];
            $category=$value['category'];
            $title=$value['title'];
            $description=$value['description'];
            $zip_code=$value['zip_code'];
            $price=$value['price'];
            $status='success';


            
            $adUpdate = new MyEntity('lepticoin', 'ad');
            $data = $adUpdate->update(

                "id_category_id = $category,
                title = '$title',
                description = '$description',
                zip_code = $zip_code,
                price='$price',
                status='$status'
                ",
                "WHERE id = 29"
            );
        
        }
        
        /**
         * @Route("/lookAd/{id}", methods={'GET'}, name="ads")
         */
        public function lookAd($value){
            $ad = $this->entity = new MyEntity('leptitcoin', 'ad');
            $data = $ad->get("*", "WHERE id = $value ", "fetchAll");
            $message = $this->entity->message;
            $dataJson = ['data' => $data, 'message' => $message];
            $json = json_encode($dataJson);
            return $json;  
            
        }
    }
?>