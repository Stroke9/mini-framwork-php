<?php
// Custom DAO ; To EDIT / CONSULT before creating another routing method !)
include dirname(__DIR__) . '/dao/EntityCRUD.class.php';
/* THINK to change DBNAME on the method, depending of your database / the one you want */

// SubRoute (for a clearer / cleaner code) => 'group of method' :
include 'Ad.class.php';


// For DEV ONLY, this class extends 'TestRoute'
include 'TestRoute.class.php';

class Route extends TestRoute
{

    /**
     * Used by the Route Controller
     * Pass the route and optionnal value and call proper method as the instanciation of the object
     */

    public $entity;
    public $result;
    public $data;
    public $subclass;

    public function __construct($arg, $val, $option){
        $this->result = $this->$arg($val, $option);
    }

    /**
     * @Route("/ads/{value?}", methods={'GET'}, name="ads")
     */
    public function ads($value = '',$option='')
    {
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $this->subclass = new Ad();
        return $this->subclass->adValider($this->entity);    // if error remettre ads 
    }

    /**
     * @Route("/adsSearch/{value?}", methods={'GET'}, name="ads")
     */
    public function adsSearch($value='',$option=''){
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $this->subclass = new Ad();
        return $this->subclass->adSearch($this->entity, $value, $option);
    }

    /**
     * @Route("/cat/{value?}", methods={'GET'}, name="Categories")
     */
    public function cat($value='',$option=''){
        $this->entity= new MyEntity('leptitcoin', 'category');
        $result = $this->entity->get("*", "", "fetchAll");
        $json = json_encode($result);
        return $json;
    }

    /**
     * @Route("/adCreation/", methods={'GET'}, name="adCreation)
     */
    public function adCreation($value,$option=''){
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $this->subclass = new Ad();
        return $this->subclass->adCreation($this->entity, $value);
    }

    /**
     * @Route("/getAdImage/", methods={'GET'}, name="getAdImage)
     */
    public function getAdImage($id,$option=''){
        $this->entity = new MyEntity('leptitcoin', 'image');
        $result = $this->entity->get("url", "WHERE id_ad_id = " . $id, "fetch");
        return json_encode($result);
    }

    /**
     * @Route("/getAdCategory/", methods={'GET'}, name="agetAdCategory)
     */
    public function getAdCategory($id,$option=''){
        $this->entity = new MyEntity('leptitcoin', 'category');
        $result = $this->entity->get("name", "WHERE id = " . $id, "fetch");
        return json_encode($result);
    }

    /**
     * @Route='/detailAd, name="detailProd"
     */
    public function detailAd($arg,$option=''){
        // REFACTOR the code with only one instance of the object, and multiple request get OR changing 'this->column'

        //RECUP INFO DANS BDD ENTITE AD
        $adData = new MyEntity('leptitcoin', 'ad');
        $ad = $adData->get("*", "WHERE id = $arg", "fetchAll");
        $ad = $ad[0];
        $idCustomer = $ad['id_customer_id'];

        //RECUP INFO DANS BDD ENTITE CUSTOMER
        $customerData = new MyEntity('leptitcoin', 'customer');
        $customer = $customerData->get("*", "WHERE id = $idCustomer", "fetchAll");
        $customer = $customer[0];
        $idAccount = $customer['id_account_id'];

        //RECUP INFO DANS BDD ENTITE IMAGE
        $imgData = new MyEntity('leptitcoin', 'image');
        $images = $imgData->get("*", "WHERE id_ad_id = $arg", "fetchAll");
        $idCategory = $ad['id_category_id'];

        //RECUP INFO DANS BDD ENTITE CATEGORY
        $categoryData = new MyEntity('leptitcoin', 'category');
        $category = $categoryData->get("*", "WHERE id = $idCategory", "fetchAll");
        $category = $category[0];

        //RECUP ID AD DANS BDD ENTITE TRANSACTION PR LA NOTE DU VENDEUR
        $transactionData = new MyEntity('leptitcoin', 'transaction');
        $transaction = $transactionData->get("*", "WHERE id_ad_id = $arg", "fetchAll");
        $transaction = $transaction[0];
        $idTransaction = $transaction['id'];

        //RECUP INFO DANS BDD ENTITE NOTE SUITE A RECUP ID TRANSACTION
        $noteData = new MyEntity('leptitcoin', 'note');
        $note = $noteData->get("*", "WHERE id_transaction_id = $idTransaction", "fetchAll");
        $note = $note[0];

        //RECUP INFO DANS BDD ENTITE ACCOUNT
        $accountData = new MyEntity('leptitcoin', 'account');
        $account = $accountData->get("*", "WHERE id = $idAccount", "fetchAll");
        $account = $account[0];


        $dataJson = ['data' => ['ad' => $ad, 'customer' => $customer, 'images' => $images, 'category' => $category, 'note' => $note, 'account' => $account], 'message' => ['message' => 'variable à ajouter']];
        $json = json_encode($dataJson);

        //var_dump($dataJson['data']);
        return $json;
    }

    /**
     * @Route("/registerCustomer/{val?}", methods={'GET'}, name="test")
     */
    public function registercustomer($val,$option=''){
        echo $val = 'coucou';
        $this->entity = new MyEntity('leptitcoin', 'category');
        $data = $this->entity->get("*");
        $json = json_encode($data);
        return $json;
    }


    /** 
     * MERGING !
     */


    /**
     * @Route("/customer/{value?}", methods={'GET'}, name="test")
     */
    public function customer($value='',$option=''){
        $this->name = $value;
        $this->entity = new MyEntity('leptitcoin', 'customer');
        $data = $this->entity->get("*", "", "fetchAll");
        $message = $this->entity->message;
        $dataJson = ['data' => $data, 'message' => $message];
        $json = json_encode($dataJson);
        return $json;
    }


    /**
     * @Route("/adminInfoPerso/{val}", methods={'POST'}, name="adCreation)
     */
    public function adminInfoPerso($value="",$option=''){
        
        $this->name = $value;
        $this->entity = new MyEntity('leptitcoin', 'account');
        $data = $this->entity->get("*", "WHERE id = " . $value, "fetchAll");
        $message = $this->entity->message;
        $dataJson = ['data' => $data, 'message' => $message];
        $jsonAdmin = json_encode($dataJson);
        return $jsonAdmin;
    }
    /**
     * @Route("/ads/{value?}", methods={'GET'}, name="ads")
     */
    public function adValider($value='',$option=''){

        $this->name = $value;
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $data = $this->entity->get("*", "WHERE status = 'success'", "fetchAll");
        $message = $this->entity->message;
        $dataJson = ['data' => $data, 'message' => $message];
        $json = json_encode($dataJson);
        return $json;
    }

    /**
     * @Route("/getAdValider/{id?}", methods={'GET'}, name="ads")
     */
    public function getAdValider($id='',$option=''){
        $entity = new MyEntity('leptitcoin','ad');
        $data = $entity->get("*", "WHERE id = ".$id." AND status = 'pending'","fetch");
        $message = $entity->message;
        return json_encode($data);
    }

    /**
     * @Route("/adPending/{value?}", methods={'GET'}, name="ads")
     */
    public function adPending($value='',$option=''){

        $this->name = $value;
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $data = $this->entity->get("*", "WHERE status = 'pending'", "fetchAll");
        $message = $this->entity->message;
        $dataJson = ['data' => $data, 'message' => $message];
        $json = json_encode($dataJson);
        return $json;
    }

    /**
     * @Route("/deleteAd/{id}", methods={'GET'}, name="ads")
     */
    public function deleteAd($id='',$option=''){
        
        echo "coucou";
        /* var_dump($id);
        $id = gettype($id) == 'array' ? json_decode($id)['value'] : $id;
        return "Entry with id '$id' was succesfuly deleted !"; */
    }


    /**
     * @Route("/adUpdate/", methods={'POST'}, name="adUpdate")
     */
    public function updateAd($value,$option=''){

        //$idAd=$value['id'];
        $category = $value['category'];
        $title = $value['title'];
        $description = $value['description'];
        $zip_code = $value['zip_code'];
        $price = $value['price'];
        $status = 'success';



        $adUpdate = new MyEntity('leptitcoin', 'ad');
        /*  $data = $adUpdate->update(

            "id_category_id = $category,
             title = '$title',
             description = '$description',
             zip_code = $zip_code,
             price='$price',
             status='$status'
            ",
            "WHERE id = 29"
        ); */
        return json_encode($value);
    }

    /**
     * @Route("/lookAd/{id}", methods={'GET'}, name="ads")
     */
    public function lookAd($value,$option=''){
        $ad = $this->entity = new MyEntity('leptitcoin', 'ad');
        $data = $ad->get("*", "WHERE id = $value ", "fetchAll");
        $message = $this->entity->message;
        $dataJson = ['data' => $data, 'message' => $message];
        $json = json_encode($dataJson);
        return $json;
    }

    /**
     * @Route= '/ad' name="annonce"
     */
    public function ad($arg,$option=''){

        $this->data = [
            "id" => 4,
            "titre" => "chat",
            "date" => "2019-08-31 10:32:23",
            "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non dolor sed ducimus eius officiis laboriosam a nam nesciunt voluptate. Dolor delectus saepe iusto mollitia est, quisquam possimus. Aspernatur, molestias debitis.
            Odit aut corrupti modi quisquam, at id enim voluptas dignissimos? Vel iusto culpa nulla aliquam in accusantium tempora nihil inventore molestias eius corrupti ratione, assumenda consequuntur excepturi incidunt sapiente ullam.
            Accusantium vitae quibusdam, repellendus unde repellat libero nulla commodi id consequatur iste aliquid dolore fuga ut autem, laborum adipisci sunt? Obcaecati modi inventore eligendi. Aperiam laudantium in commodi quam a?
            Pariatur iusto delectus autem eveniet cupiditate earum fugiat sunt nisi perferendis vel vero repellendus consectetur tempora nostrum consequuntur, tempore asperiores aut, beatae deleniti animi quam harum blanditiis molestias! Quam, fuga.
            Voluptatem repellendus expedita omnis quia optio facere veritatis explicabo eius, illo nisi nam hic commodi, eligendi minus. Necessitatibus, at! Enim facilis nihil sequi quam nisi harum incidunt sed assumenda in!",
            "id_customer" => 6,
            "id_category" => 6,
            "zip_code" => 6543,
            "view" => 4,
            "bookmarked" => 4,
            "price" => "45"
        ];
        echo json_encode($this->data);
    }
}
