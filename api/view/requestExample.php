<?php

/**
 * Dans la SESSION seront stockée des informations tels que "is_logged" (si connectée), filtre sur les annonces tel que : lieu, catégories, ordre de classement et autres...
 */
$uriParsed = 'annonces';
$title = strtoupper(substr($uriParsed, 0, 1)) . substr($uriParsed, 1);

/**
 * Aussi, des variables seront instanciée permettant de génerer un premier 'rendu' comme par exemple une variable qui stockerais le nom de l'utilisateur, et qui sera utilisée au travers de notre 'moteur de template' maison
 */


/**
 * Ces variables (cf. higher comment) viendront d'une première requête instanciée par notre script php
 */

// INIT cURL
$curl = curl_init();
$url = __DIR__ . '/api1.php?action=json&value=10';
$url = str_replace('/var/www/html', 'localhost', $url);

// Configuration
$curl_options = array(
    CURLOPT_URL => $url,
    CURLOPT_HEADER => false,
    CURLOPT_RETURNTRANSFER => true
);

curl_setopt_array($curl, $curl_options);

// Execution
$myjson = curl_exec($curl);

// END (Close)
curl_close($curl);

//OUTPUT : 
var_dump($myjson);
$jsonDecoded = json_decode($myjson, true);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?> Examples</title>
</head>

<body>
    <h1>Welcome to <?php echo $title; ?> :</h1>
    <div id="load">
        <h2>From Server (onload) : </h2>
        <?php
        if (isset($jsonDecoded) && !empty($jsonDecoded)) {
            foreach ($jsonDecoded as $prop => $value) {
                echo '<pre>' . $prop . ' => ' . $value . '</pre>';
            }
        }
        ?>
    </div>
    <div id="output">
        <h2>From api (ondemand) : </h2>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            myAjax();
        });

        function myAjax() {
            var xhttp = new XMLHttpRequest();
            xhttp.open('GET', 'api1.php?action=json&value=5');
            xhttp.send();

            xhttp.onreadystatechange = function() {
                if (this.status == 200 && this.readyState == 4) {
                    var mydiv = document.getElementById('output');
                    var myjson = JSON.parse(this.responseText);
                    mydiv.innerHTML += `<h3>${typeof myjson} : </h3>`;
                    for (let prop in myjson) {
                        mydiv.innerHTML += `<pre>${prop} => ${myjson[prop]}</pre>`;
                    }
                    /* 
                    document.getElementById('output').innerHTML='Retour de l\'api : \n'+this.responseText; */
                    console.log();
                }
            }
        }
    </script>
</body>

</html>