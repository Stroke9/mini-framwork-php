<?php

    $curl = curl_init();
    $url=__DIR__.'/api.php?action=customer';
    $url=str_replace($replaceRoot,$replaceHost,$url);
  
    // Configuration
    $curl_options=array(
        CURLOPT_URL=>$url,
        CURLOPT_HEADER=>false,
        CURLOPT_RETURNTRANSFER=>true
    );

    curl_setopt_array($curl,$curl_options);
    // Execution
    $myjson=curl_exec($curl); 

    // END (Close)
    curl_close($curl);

    //OUTPUT : 
    var_dump($myjson);
    $jsonDecoded=json_decode($myjson,true);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <?php

        if(isset($jsonDecoded['message'])) {
            foreach($jsonDecoded['message'] as $key => $value){
                if(count($jsonDecoded['message'][$key]) != 0){
                    echo '<div class="alert alert-'.$key.'" role="alert">';
                        echo '<ul>';
                        for($i = 0; $i < count($jsonDecoded['message'][$key]); $i++) {
                            echo '<li>';
                            echo $value[$i];
                            echo '</li>';
                            }
                        echo '</ul>';  
                    echo '</div>';
                }
            }
        }

        foreach($jsonDecoded['data'] as $array) {
            echo "<h1>Bonjour je m'appel " . $array['id'] . "</h1>";
            echo "<p>" . $array['bio'] . "</p>";
        };
    ?>
    
</body>
</html>