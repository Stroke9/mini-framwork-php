-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 18 Octobre 2019 à 10:59
-- Version du serveur :  5.7.27-0ubuntu0.18.04.1
-- Version de PHP :  7.2.22-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `leptitcoin`
--

-- --------------------------------------------------------

--
-- Structure de la table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `account`
--

INSERT INTO `account` (`id`, `email`, `pseudo`, `password`, `role`) VALUES
(81, 'ch@gmailk.com', 'srntueitrue', '123', 'customer'),
(82, 'janine@gmail.com', 'aueiuei', '123', 'customer'),
(83, '', '', '', 'customer'),
(84, 'janine@gmail.commm', 'admin', '123', 'customer'),
(85, 'clem@gmail.com', 'a=ueiuie', '123', 'customer');

-- --------------------------------------------------------

--
-- Structure de la table `ad`
--

CREATE TABLE `ad` (
  `id` int(11) NOT NULL,
  `id_customer_id` int(11) NOT NULL,
  `id_category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` int(11) NOT NULL,
  `views` int(11) DEFAULT NULL,
  `bookmarked` int(11) DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `ad`
--

INSERT INTO `ad` (`id`, `id_customer_id`, `id_category_id`, `title`, `date`, `description`, `zip_code`, `views`, `bookmarked`, `price`, `status`) VALUES
(1, 6, 122, 'Vend voiture toute neuve', '2019-10-09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at erat condimentum ligula iaculis volutpat nec a nisl. Integer ligula turpis, blandit nec posuere id, tempus eget justo. Donec sem velit, vestibulum eget dolor sed, blandit dictum nunc. Nunc imperdiet tortor vel imperdiet pulvinar. Maecenas sed sapien ac tellus viverra posuere. Nulla augue dolor, gravida a dolor vel, interdum condimentum nibh. Curabitur quis enim interdum, iaculis tellus vel, ornare est.\r\n\r\nEtiam rutrum quam ac tristique aliquet. Pellentesque vel odio nec nisl sodales eleifend sed vel odio. Pellentesque in justo in neque ullamcorper sollicitudin malesuada ut diam. Cras pellentesque vitae ipsum sed ullamcorper. Sed molestie, enim at consequat suscipit, elit lectus luctus nulla, nec laoreet quam est eu velit. Vivamus efficitur sagittis eros in fermentum. Curabitur scelerisque ante in dui suscipit ornare. Etiam quis tristique ligula. Donec pharetra ipsum at bibendum euismod. Suspendisse aliquet tortor ac arcu aliquet eleifend. Mauris facilisis porta bibendum. Duis lobortis tellus id cursus mattis. Ut ac malesuada nisi.\r\n\r\nMauris sed porta mi. Phasellus facilisis enim in mattis facilisis. Donec vitae felis rhoncus ipsum consequat sollicitudin nec et augue. Pellentesque euismod dolor quam, non mattis libero bibendum nec. Aliquam condimentum at ligula eu tempus. Integer ullamcorper, neque non iaculis pharetra, eros diam ornare dui, a venenatis libero lacus quis velit. Suspendisse potenti. Aliquam vulputate elit eget semper pulvinar. Sed sed dolor scelerisque, rutrum ante a, iaculis enim. Proin porta nibh id libero blandit, dignissim faucibus justo elementum.', 34000, 34, 2, '34,12', ''),
(2, 43, 122, 'Vend voiture toute pourrie !', '2019-10-17', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at erat condimentum ligula iaculis volutpat nec a nisl. Integer ligula turpis, blandit nec posuere id, tempus eget justo. Donec sem velit, vestibulum eget dolor sed, blandit dictum nunc. Nunc imperdiet tortor vel imperdiet pulvinar. Maecenas sed sapien ac tellus viverra posuere. Nulla augue dolor, gravida a dolor vel, interdum condimentum nibh. Curabitur quis enim interdum, iaculis tellus vel, ornare est.\r\n\r\nEtiam rutrum quam ac tristique aliquet. Pellentesque vel odio nec nisl sodales eleifend sed vel odio. Pellentesque in justo in neque ullamcorper sollicitudin malesuada ut diam. Cras pellentesque vitae ipsum sed ullamcorper. Sed molestie, enim at consequat suscipit, elit lectus luctus nulla, nec laoreet quam est eu velit. Vivamus efficitur sagittis eros in fermentum. Curabitur scelerisque ante in dui suscipit ornare. Etiam quis tristique ligula. Donec pharetra ipsum at bibendum euismod. Suspendisse aliquet tortor ac arcu aliquet eleifend. Mauris facilisis porta bibendum. Duis lobortis tellus id cursus mattis. Ut ac malesuada nisi.\r\n\r\nMauris sed porta mi. Phasellus facilisis enim in mattis facilisis. Donec vitae felis rhoncus ipsum consequat sollicitudin nec et augue. Pellentesque euismod dolor quam, non mattis libero bibendum nec. Aliquam condimentum at ligula eu tempus. Integer ullamcorper, neque non iaculis pharetra, eros diam ornare dui, a venenatis libero lacus quis velit. Suspendisse potenti. Aliquam vulputate elit eget semper pulvinar. Sed sed dolor scelerisque, rutrum ante a, iaculis enim. Proin porta nibh id libero blandit, dignissim faucibus justo elementum.', 65000, 444, 44, '4444444', 'pending');

-- --------------------------------------------------------

--
-- Structure de la table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street_number` int(11) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complementary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `address`
--

INSERT INTO `address` (`id`, `street`, `street_number`, `zip_code`, `city`, `complementary`, `country`) VALUES
(45, 'rerer', 21, 65000, 'Tarbes', '', 'France'),
(46, 'rerer', 23, 65000, 'Tarbes', '', 'France'),
(47, 'rerer', 25, 65000, 'Tarbes', '', 'France'),
(48, 'rerer', 25, 65000, 'Tarbes', '', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `id_account_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `name`, `id_parent`) VALUES
(122, 'Voiture', NULL),
(123, 'Immo', NULL),
(124, 'Informatique', NULL),
(125, 'Enfant', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `id_account_id` int(11) NOT NULL,
  `id_adress_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `favorite_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profil_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `customer`
--

INSERT INTO `customer` (`id`, `id_account_id`, `id_adress_id`, `name`, `fname`, `favorite_place`, `bio`, `profil_picture`, `rank`, `identity_document`, `phone`, `token_google`, `token_facebook`) VALUES
(43, 81, 45, 'Janime', 'Janime', NULL, NULL, 'defaultPP.jpg', 'Non certifiÃ©', 'Transmit, en cours de traitement', NULL, NULL, NULL),
(44, 82, 46, 'Janime', 'Janime', NULL, NULL, 'defaultProfil.jpg', 'Non certifiÃ©', 'Non Transmit', NULL, NULL, NULL),
(45, 84, 47, 'Janime', 'Janime', NULL, NULL, 'defaultProfil.jpg', 'Non certifiÃ©', 'Non Transmit', NULL, NULL, NULL),
(46, 85, 48, 'Janime', 'Janime', NULL, NULL, 'defaultProfil.jpg', 'Non certifiÃ©', 'Non Transmit', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `id_ad_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `id_ad_id`, `url`) VALUES
(1, 2, 'cat1.jpg'),
(3, 2, 'cat2.jpg'),
(4, 2, 'cat3.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `id_customer_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id`, `id_customer_id`, `date`, `content`) VALUES
(1, 43, '2019-10-17 00:00:00', 'c\'est genial !');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191008091031', '2019-10-08 09:12:29'),
('20191008093011', '2019-10-08 09:30:22'),
('20191008102148', '2019-10-08 10:22:05'),
('20191008143423', '2019-10-08 14:34:41'),
('20191009102354', '2019-10-09 10:24:00');

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE `note` (
  `id` int(11) NOT NULL,
  `id_transaction_id` int(11) NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `note`
--

INSERT INTO `note` (`id`, `id_transaction_id`, `note`) VALUES
(1, 1, '5');

-- --------------------------------------------------------

--
-- Structure de la table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `id_ad_id` int(11) NOT NULL,
  `id_customer_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `transaction`
--

INSERT INTO `transaction` (`id`, `id_ad_id`, `id_customer_id`, `message_id`, `status`, `date_start`, `date_end`) VALUES
(1, 2, 43, 1, 'pending', '2019-10-17 00:00:00', '2019-10-17 00:00:12');

-- --------------------------------------------------------

--
-- Structure de la table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `id_customer_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wishlist_items`
--

CREATE TABLE `wishlist_items` (
  `id` int(11) NOT NULL,
  `id_wishlist_id` int(11) NOT NULL,
  `id_ad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ad`
--
ALTER TABLE `ad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UNIQ_77E0ED588B870E04` (`id_customer_id`) USING BTREE,
  ADD KEY `UNIQ_77E0ED58A545015` (`id_category_id`) USING BTREE;

--
-- Index pour la table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_880E0D763EE1DF6D` (`id_account_id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UNIQ_81398E093EE1DF6D` (`id_account_id`) USING BTREE,
  ADD KEY `UNIQ_81398E094B3458B` (`id_adress_id`) USING BTREE;

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `UNIQ_C53D045FA1B194A6` (`id_ad_id`) USING BTREE;

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B6BD307F8B870E04` (`id_customer_id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFBDFA1412A67609` (`id_transaction_id`);

--
-- Index pour la table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_723705D18B870E04` (`id_customer_id`),
  ADD UNIQUE KEY `UNIQ_723705D1A1B194A6` (`id_ad_id`),
  ADD KEY `IDX_723705D1537A1329` (`message_id`);

--
-- Index pour la table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9CE12A318B870E04` (`id_customer_id`);

--
-- Index pour la table `wishlist_items`
--
ALTER TABLE `wishlist_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B5BB81B5E39C993A` (`id_wishlist_id`),
  ADD UNIQUE KEY `UNIQ_B5BB81B5A1B194A6` (`id_ad_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT pour la table `ad`
--
ALTER TABLE `ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT pour la table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `note`
--
ALTER TABLE `note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `wishlist_items`
--
ALTER TABLE `wishlist_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ad`
--
ALTER TABLE `ad`
  ADD CONSTRAINT `FK_77E0ED58A545015` FOREIGN KEY (`id_category_id`) REFERENCES `category` (`id`);

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `FK_880E0D763EE1DF6D` FOREIGN KEY (`id_account_id`) REFERENCES `account` (`id`);

--
-- Contraintes pour la table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `FK_81398E093EE1DF6D` FOREIGN KEY (`id_account_id`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `FK_81398E094B3458B` FOREIGN KEY (`id_adress_id`) REFERENCES `address` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045FA1B194A6` FOREIGN KEY (`id_ad_id`) REFERENCES `ad` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F8B870E04` FOREIGN KEY (`id_customer_id`) REFERENCES `customer` (`id`);

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_CFBDFA1412A67609` FOREIGN KEY (`id_transaction_id`) REFERENCES `transaction` (`id`);

--
-- Contraintes pour la table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `FK_723705D1537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FK_723705D18B870E04` FOREIGN KEY (`id_customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `FK_723705D1A1B194A6` FOREIGN KEY (`id_ad_id`) REFERENCES `ad` (`id`);

--
-- Contraintes pour la table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `FK_9CE12A318B870E04` FOREIGN KEY (`id_customer_id`) REFERENCES `customer` (`id`);

--
-- Contraintes pour la table `wishlist_items`
--
ALTER TABLE `wishlist_items`
  ADD CONSTRAINT `FK_B5BB81B5A1B194A6` FOREIGN KEY (`id_ad_id`) REFERENCES `ad` (`id`),
  ADD CONSTRAINT `FK_B5BB81B5E39C993A` FOREIGN KEY (`id_wishlist_id`) REFERENCES `wishlist` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
