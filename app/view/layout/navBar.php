<style>
  /* Snippet from codepen -- by Craig Watson  */
  /* Nav / Navbar Styles */
  .navbar {
    /* background: #ffffff !important; */
    padding: 30px 20px;
    transition-duration: 1s;
    color: #000 !important;
  }

  .navbar a {
    color: #000 !important;
  }

  .navbar-default .navbar-brand,
  .navbar-default .navbar-nav>li>a {
    color: #f00;
  }

  /* jQuery Styles
  ===================================*/
  .navbar-scroll {
    background: #2e3031 !important;
    padding: 30px 20px;
    box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.4);
    transition-duration: 1s;
    color: #fff !important;
  }

  .navbar-scroll a {
    color: #fff !important;
  }

  .nav-dropdown-scroll {
    background: #ffffff;
    box-shadow: 0px 10px 9px rgba(0, 0, 0, 0.4);
  }
</style>

<!-- NAV BAR component -->

<nav class="navbar navbar-default sticky-top navbar-expand-lg navbar-light bg-light static-top">
  <!-- navbar-light bg-light -->
  <a class="navbar-brand" href="<?php echo $_SESSION['ENV']['root_uri'] . '/public/index.php'; ?>"><img width="80" height="80" src="<?php echo $_SESSION['ENV']['root_uri'] . '/public/img/logo/lpc.png'; ?>" class="d-inline-block align-top" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav ">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $rootScript; ?>">Accueil <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $rootScript . "/ad"; ?>">Annonces</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $rootScript . "/personalBoard"; ?>">Compte</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          A propos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?php echo $rootScript . "/about" . "/us"; ?>">Notre équipe</a>
          <a class="dropdown-item" href="<?php echo $rootScript . "/about" . "/cgu"; ?>">CGU</a>
          <a class="dropdown-item" href="<?php echo $rootScript . "/about" . "/contact"; ?>">Nous contacter !</a>
        </div>
      </li>
    </ul>
  </div>
</nav>

<script>
  /* Snippet from codepen -- by Craig Watson  */
  //Change pos/background/padding/add shadow on nav when scroll event happens 
  $(function() {
    var navbar = $('.navbar');

    $(window).scroll(function() {
      if ($(window).scrollTop() <= 60) {
        navbar.removeClass('navbar-scroll');
      } else {
        navbar.addClass('navbar-scroll');
      }
    });
  });
  /* End -- snipet */
</script>