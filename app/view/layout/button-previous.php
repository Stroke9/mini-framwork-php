<?php 
if(isset($_SESSION['history'])){
    $lastPage = count($_SESSION['history']) > 1 ? count($_SESSION['history']) - 2 : 0;
    $previous = $_SESSION['history'][$lastPage];
}else{
    $previous = '';
}
?>

<a class="btn btn-primary m-3" href="<?php echo $previous; ?>" role="button">< Go Back</a>