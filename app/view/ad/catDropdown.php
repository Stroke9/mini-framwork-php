<div class="col-3">
    <div class="col-8">
        <div class="input-group input-group-lg">
            <?php 
            /* Verify if cat are set & are not empty from BDD */

            if(isset($cat)){

                if(!empty($cat) && $cat != false){
                ?>
                <select name="category" class="custom-select bg-primary text-white" id="inputGroupSelect01 py-3">
                    <option selected disabled>Categorie</option>
                    <?php 
                        foreach($cat as $caty){
                    ?>
                        <!-- MAKE item list of our categories from the database -->
                        <option class="bg-white text-dark" value="<?php echo $caty['name'];?>"><?php echo $caty['name'];?></option>
                        <?php 
                            if(end($cat) !== $caty){
                        ?> 
                            <div class="dropdown-divider"></div>
                        <?php 
                        } // end -- if 
                        ?>
                    <?php 
                    }
                    ?>
                    </select>
                </div>
                <?php 
                }
            } // end -- foreach
            ?>
    </div>
    </div>
