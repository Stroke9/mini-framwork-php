<?php
include '../app/model/curlRequestApi.php';
// $_SESSION['COMPONENT']['dropdown']=$cat; // IDEA to implement angular-component like in php with SESSION;

/** FETCH ALL DATA from Database */
    
    // ADS
    $search = $_POST['search'] ?? false;
    $send = $_POST['send'] ?? false;
    @$option = ['title' => $_POST['title'], 'category' => $_POST['category'], 'place' => $_POST['place']];
    $ads = curlRequest('adsSearch', $search, $option);

    // MINIATURES FOR ADS
    $images = [];
    foreach($ads['data'] as $ad){
        $images[$ad['id']] = curlRequest('getAdImage',$ad['id']);
    }

    $categories = [];
    foreach($ads['data'] as $ad){
        $categories[$ad['id']]=curlRequest('getAdCategory',$ad['id_category_id']);
    }

    // CATEGORIES
    $cat=curlRequest('cat');
?>

<?php
$nbElements = 10;
$count = count($ads['data']);
$nbPage = ceil($count / $nbElements);

function pagination($ads, $nbElements = 10)
{
    $pageCible = @$_GET['p'];
    $first = ($pageCible * $nbElements);
    $result = array_slice($ads, $first, $nbElements);
    return $result;
}


$adsForPage = pagination($ads['data']);

//var_dump($adsForPage);
?>


<div class="col-6 offset-3 mt-5">
    <!-- SEARCH BAR - COMPONENT -->
    <div class="row border p-5 bg-white rounded shadow-lg">

        <!--  CATEGORY DROP DOWN - COMPONENT -->
        <?php
        include __DIR__ . '/catDropdown.php';
        ?>

        <!-- SEARCH AREA -->
        <div class="col-6 input-group">
            <input class="form-control form-control-lg rounded-pill text-center" type="text" placeholder="Que recherchez vous ?">
        </div>

        <!-- PLACE INDICATOR -->
        <div class="col-3">
            <div class="row">
                <div class="col-6"></div>
                <div class="col-6">
                    <input class="form-control form-control-lg rounded-pill text-center" type="text" placeholder="Lieu">
                </div>
            </div>
        </form> 
    </div>
</div>
</div>
<!-- END -- SEARCH BAR -->

<div class="col-8 offset-2 mt-5">
    <!-- RESULT OF THE RESEARCH -->
    <div class=my-5>
        <div class="row">
            <div class="col-6 my-auto">Résultat trouvé : <?php echo $count ?> </div>
            <div class="col-6 text-right">
                <a class="btn btn-outline-primary dropdown-toggle text-center shadow-lg" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trier par</a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">Prix Croisssant</a>
                    <a class="dropdown-item" href="#">Prix Décroisssant</a>
                    <a class="dropdown-item" href="#">Récent</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RESULT CARD - COMPONENT -->
<div class="col-8 offset-2 mt-5">
    <?php
    foreach ($adsForPage as $ad) {
        $imgSrc = 'http://localhost' . $_SESSION['ENV']['root_uri'] . '/public/img/examples/upload/' . $images[$ad['id']]['url'];
        $imgInfo = pathinfo($imgSrc);
        $imgSrc = isset($imgInfo['extension']) ? $imgSrc : $_SESSION['ENV']['root_uri'] . '/public/img/examples/upload/' . 'cat3.jpg';
        ?>

        <button class="delete-decoration btn-block btn bg-white shadow-lg mb-3 p-3">
                <a href="<?php echo $_SESSION['ENV']['root_uri'] . "/public/index.php/detailAd/" . $ad['id'] ?>" class="btn">
            <div class="row">

                <!-- ZONE IMAGE ANNONCE -->
                <div class="col-4">
                    <img class="img-thumbnail" src="<?php echo $imgSrc; ?>" alt="chat">
                </div>

                <!-- ZONE ANNONCE TITRE PRIX LIEU.. -->
                <div class="col-7">
                    <div class="row">
                        <div class="col-9 text-left">
                            <h3><?php echo $ad['title']; ?></h3>
                        </div>
                        <div class="col-3 text-right">
                            <h3><?php echo $ad['price']; ?> Eur</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 mt-5 text-left">
                            <h4><?php echo $categories[$ad['id']]['name']; ?></h4><br>
                            <h4><?php echo $ad['zip_code'] . ' Villeeeee' ?></h4>
                        </div>
                        <div class="col-6 mt-5 text-right">
                            <br><br>
                            <h5><?php echo $ad['date']; ?></h5>
                        </div>
                    </div>
                </div>
                </a>
        </button>
                <!-- ZONE COEUR FAVORI -->
                <div class="col-1 text-right">
                    <a href="#"><img src="<?php echo $_SESSION['ENV']['root_uri'] . '/public/img/ico/favori.png' ?>" width="40px" height="40px"></a>
                </div>
            </div>
    <?php
    }   // end -- foreach
    ?>
</div>
<!-- Pagination -->
<div class="col-6 offset-3 p-5 mt-5 border-top pagination">
    <p class="mx-auto">
        <?php for ($i = 0; $i < $nbPage; $i++) { ?>
            <a href="<?php echo $_SERVER['PHP_SELF'] . '?p=' . $i; ?>"><?php echo ($i + 1); ?></a>
        <?php } ?>
    </p>
</div>