<?php

include '../api/model/dao/EntityCRUD.class.php';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    $email = $_POST['email'];
    $pseudo = $_POST['pseudo'];
    $emailConfirm = $_POST['emailConfirm'];
    $message = ["danger" => [], "success" => []];
    
    $dataRegister = ['email' => $email, 'pseudo' => $pseudo];

    foreach ($dataRegister as $key => $val) {
        $account = new MyEntity('leptitcoin', 'account');
        $result = $account->get("$key", "WHERE $key = '$val'");
        if($dataRegister[$key] == "") {
            array_push($message["danger"], "Le champ " . $key . " doit etre completé !");
        } elseif($result){
            array_push($message["danger"], $key . " deja existant ...");
        }
    }

    if($email !== $emailConfirm) {
        array_push($message["danger"], "Les emails ne sont pas identiques");
    }
    
    if(empty($message['danger'])) {
        $_SESSION['dataRegister1'] = $dataRegister;
        header("Location: register2");
    }
}
