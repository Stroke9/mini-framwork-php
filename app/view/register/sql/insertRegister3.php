<?php
include '../api/model/dao/EntityCRUD.class.php';
// verif aussi si link direct sur register3 : gestion erreur

if($_SERVER['REQUEST_METHOD'] == 'POST') {
      
    $idRecto = $_FILES['idRecto'];
    $idVerso = $_FILES['idVerso'];

    $message = ["danger" => [], "success" => []];
    $dataRegister = ['ID recto' => $idRecto, 'ID verso' => $idVerso];

    function validIdentityDocument() {
        global $message;
        global $dataRegister;

        foreach ($dataRegister as $key => $val) {
            if($dataRegister[$key]['name'] == "") {
                array_push($message["danger"], "Le fichier " . $key . " n'a pas été ajouté ou n'a pas été télechargé correctement");
            } elseif ($dataRegister[$key]['type'] !== "image/jpg" && $dataRegister[$key]['type'] !== "image/jpeg" && $dataRegister[$key]['type'] !== "image/png" && $dataRegister[$key]['type'] !== "image/pdf"){   
                array_push($message["danger"], "La format " . $dataRegister[$key]['type'] . " de ". $key . " n'est pas supporté");
            }
            if ($dataRegister[$key]['size'] > 500000) {
                array_push($message["danger"], "La taille du fichier " . $key . " est superieur a 500 ko");
            }
        }
    }


    function AutoFix($file, $fileName) {

        if (!@$test = exif_read_data($file)) {
            $exif = '';
        } else {
            $exif = exif_read_data($file);
        }
        $type = exif_imagetype($file);
        $allowedTypes = array(1, 2, 3, 6);
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case 1 :
                $img = imageCreateFromGif($file);
                break;
            case 2 :
                $img = imageCreateFromJpeg($file);
                break;
            case 3 :
                $img = imageCreateFromPng($file);
                break;
            case 6 :
                $img = imageCreateFromBmp($file);
                break;
        }
        if ($img && $exif && isset($exif['Orientation']))
        {
            $ort = $exif['Orientation'];
            if ($ort == 6 || $ort == 5)
                $img = imagerotate($img, 270, null);
            if ($ort == 3 || $ort == 4)
                $img = imagerotate($img, 180, null);
            if ($ort == 8 || $ort == 7)
                $img = imagerotate($img, 90, null);
            if ($ort == 5 || $ort == 4 || $ort == 7)
                imageflip($img, IMG_FLIP_HORIZONTAL);
            if (!$exif) {
                imageflip($img, IMG_FLIP_HORIZONTAL);
            }
            $img = imagejpeg($img, './uploads/' . $fileName, 50);
        } else {
            $img = imagejpeg($img, './uploads/' . $fileName, 50);
        }
        return $img;
    }
          
    function sendToBdd($checkIdentity) {

        if (empty($message["danger"])) {

            session_start();
    
            $email = $_SESSION['dataRegister1']['email'];
            $pseudo = $_SESSION['dataRegister1']['pseudo'];
    
            $password = $_SESSION['dataRegister2']['mot de passe'];
            $name = $_SESSION['dataRegister2']['nom'];
            $fname = $_SESSION['dataRegister2']['prenom'];
            $nb_street = $_SESSION['dataRegister2']['numero de rue'];
            $street = $_SESSION['dataRegister2']['rue'];
            $zip_code = $_SESSION['dataRegister2']['code postal'];
            $city = $_SESSION['dataRegister2']['ville'];
            $country = $_SESSION['dataRegister2']['pays'];
    
            if (isset($_SESSION['dataRegister2']['complement adresse'])) {
                $complementary = $_SESSION['dataRegister2']['complement adresse'];
            } else {
                $complementary = '';
            }
    
            $account = new MyEntity('leptitcoin', 'account');
            $checkSend = $account->get("pseudo", "WHERE pseudo = '$pseudo'");

            if (!$checkSend) {
                $account->post("`email`,`pseudo`,`password`,`role`", "'$email', '$pseudo', '$password', 'customer'");
                $idAccount = $account->bdd->bdd->lastInsertId();
        
                $address = new MyEntity('leptitcoin', 'address');
                $address->post("`street`, `street_number`, `zip_code`, `city`, `complementary`, `country`", "'$street', '$nb_street', '$zip_code', '$city', '$complementary', '$country'");
                $idAddress = $address->bdd->bdd->lastInsertId();
        
                $customer = new MyEntity('leptitcoin', 'customer');
                $customer->post("`id_account_id`, `id_adress_id`, `name`, `fname`, `profil_picture`, `rank`, `identity_document`", "'$idAccount', '$idAddress', '$name', '$fname', 'defaultProfil.jpg', 'Non certifié', '$checkIdentity'");
            }
        }
    }

    if(isset($_POST['identityIgnore'])) {
        sendToBdd('Non Transmit');
        array_push($message['success'], "Votre compte a bien été créer !");
        //header('Refresh: 4; URL=register1.php');
        
    } else {
        $uniqueId = md5(uniqid())."jpeg";
        validIdentityDocument();
        AutoFix($idRecto, $uniqueId);
        sendToBdd('Transmit, en cours de traitement');
        array_push($message['success'], "Votre compte a bien été créer !");
        //header('Refresh: 4; URL=register1.php');
    }
}
    