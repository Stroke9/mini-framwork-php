<?php

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $name = $_POST['name'];
    $fname = $_POST['fname'];
    $password = $_POST['password'];
    $passwordConfirm = $_POST['passwordConfirm'];
    $nb_street = $_POST['nb_street'];
    $street = $_POST['street'];
    $complementary = $_POST['complementary'];
    $zip_code = $_POST['zip_code'];
    $city = $_POST['city'];
    $country = $_POST['country'];
    $message = ["danger" => [], "success" => []];

    if(!$complementary) {
        $dataRegister = ['nom' => $name, 'prenom' => $fname, 'mot de passe' => $password, 'numero de rue' => $nb_street, 'rue' => $street, 'code postal' => $zip_code, 'ville' => $city, 'pays' => $country];
    } else {
        $dataRegister = ['nom' => $name, 'prenom' => $fname, 'mot de passe' => $password, 'numero de rue' => $nb_street, 'rue' => $street, $complementary => 'complement adresse', 'code postal' => $zip_code, 'ville' => $city, 'pays' => $country];
    }

    foreach ($dataRegister as $key => $val) {
        if($dataRegister[$key] == "") {
            array_push($message["danger"], "Le champ " . $key . " doit etre completé !");
        }
    }

    if($password !== $passwordConfirm) {
        array_push($message["danger"], "Les mots de passes ne sont pas identiques !");
    }
    
    if(empty($message['danger'])) {
        $_SESSION['dataRegister2'] = $dataRegister;
        header("Location: register3");
    }
}