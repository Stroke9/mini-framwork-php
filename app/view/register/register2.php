<?php 
    include "../app/view/register/sql/insertRegister2.php";
    if(isset($message)) {
        foreach($message as $key => $value){
            if(count($message[$key]) != 0){
                echo '<div class="alert alert-'.$key.'" role="alert">';
                    echo '<ul>';
                    for($i = 0; $i < count($message[$key]); $i++) {
                        echo '<li>';
                        echo $value[$i];
                        echo '</li>';
                        }
                    echo '</ul>';  
                echo '</div>';
            }
        }
    }
?>

<div class="bg-info">
    <div class="row">
        <div class="col-8 offset-2 mb-5 border p-5 bg-light mt-5 rounded-lg">
            <h3 class="text-center mb-3">Informations personnelles</h3>
            <span class="font-weight-bold col-10 offset-1 text-danger">*</span><small class="text-danger"> Champs requis</small>
            <div  class="col-10 offset-1">
                <form method="POST">
                    <div class="row">
                        <div class="form-group col-6">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="name" id='name'>
                        </div>
                        <div class="form-group col-6">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="fname" placeholder="* Prenom">
                        </div>
                        <div class="form-group col-6">
                            <input type="password" class="form-control form-control-lg rounded-pill" name="password" placeholder="* Mot de passe">
                        </div>
                        <div class="form-group col-6">
                            <input type="password" class="form-control form-control-lg rounded-pill" name="passwordConfirm" placeholder="* Confirmer mot de passe">
                        </div>
                        <div class="form-group col-3">
                            <input type="text" class="form-control form-control-lg rounded-pill"name="nb_street" placeholder="* Num">
                        </div>
                        <div class="form-group col-9">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="street" placeholder="* Rue">
                        </div>
                        <div class="form-group col-12">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="complementary" placeholder="Complement d'adresse">
                        </div>
                        <div class="form-group col-6">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="zip_code" placeholder="* Code postal">
                        </div>
                        <div class="form-group col-6">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="city" placeholder="* Ville">
                        </div>
                        <div class="form-group col-6">
                            <input type="text" class="form-control form-control-lg rounded-pill" name="country" placeholder="* Pays" value="France">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block rounded-pill">Poursuivre inscription</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>












<div class="col-10 offset-1">
    <div class="text-center mt-5 col-10">
        <p>Etape 2/3</p>
    </div>
    <div class="row">
        <div class="col-5 mt-2 border-right">
            <h3 class="mb-5 text-center">Pourquoi creer un compte ?</h3>
            <div  class="col-10 offset-1">
                <div class="card">
                    <h5 class="card-header">Bienvenue dans lepticoin</h5>
                    <div class="card-body bg-primary">
                        <p class="card-text text-white">Some quick example text to build on the card title and make up the bulk of the card's content.
                        nous somme fort blablabla les meilleurs blablabla</p>
                    </div>
                </div>
            </div> 
        </div>
        
            
    </div>
</div>