<?php
    /* include "../app/view/register/sql/insertRegister1.php"; */
    if(isset($message)) {
        foreach($message as $key => $value){
            if(count($message[$key]) != 0){
                echo '<div class="alert alert-'.$key.'" role="alert">';
                    echo '<ul>';
                    for($i = 0; $i < count($message[$key]); $i++) {
                        echo '<li>';
                        echo $value[$i];
                        echo '</li>';
                        }
                    echo '</ul>';  
                echo '</div>';
            }
        }
    }
?>
<div class="bg-info shadow-lg">
    <div class="row">
        <div class="col-6 offset-3 mb-5 border p-5 bg-light mt-5 rounded-lg shadow-lg">
            <div class="row">
                <div class="col-6 mt-2 border-right">
                    <h3 class="text-center">Pourquoi créer un compte ?</h3>
                    <div class="col-6 offset-3 mt-4 mb-5 border border-primary"></div>
                    <div  class="col-10 offset-1 mt-5">
                        <div class="card my-5">
                            <h5 class="card-header text-center">Bienvenue dans Le Ptit Coin</h5>
                            <div class="card-body">
                                <ul>
                                    <li>Rapidité</li>
                                    <li>Gage de confiance</li>
                                    <li>Sécurité</li>
                                </ul>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-6 mt-2 border-left">
                    <h3 class="text-center ">Creer un compte</h3>
                    <div class="col-6 offset-3 mt-4 mb-5 border border-primary"></div>
                    <span class="font-weight-bold col-10 offset-1 text-danger">*</span><small class="text-danger"> Champs requis</small>
                    <div  class="col-10 offset-1">
                        <form method="POST">
                            <div class="form-group">
                                <input type="email" class="form-control form-control-lg rounded-pill" placeholder="* Email" name="email">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-control-lg rounded-pill" placeholder="* Email" name="emailConfirm">
                            </div>
                            <div class="form-group mb-5">
                                <input type="text" class="form-control form-control-lg rounded-pill" name="pseudo" placeholder="* Pseudo">
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-lg btn-block rounded-pill">Creer mon Compte</button>
                            </div>
                        </form>
                        <div class="border my-5"></div>
                        <div>
                            <button class="btn btn-danger btn-lg btn-block rounded-pill mb-3" href="">Se connecter avec Google</button>
                            <button class="btn btn-info btn-lg btn-block rounded-pill" href="">Se connecter avec Facebook</button>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>