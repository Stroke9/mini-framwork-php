<?php 
    /* include "../app/view/register/sql/insertRegister3.php"; */
    if(isset($message)) {
        foreach($message as $key => $value){
            if(count($message[$key]) != 0){
                echo '<div class="alert alert-'.$key.'" role="alert">';
                    echo '<ul>';
                    for($i = 0; $i < count($message[$key]); $i++) {
                        echo '<li>';
                        echo $value[$i];
                        echo '</li>';
                        }
                    echo '</ul>';  
                echo '</div>';
            }
        }
    }
?>



<div class="bg-info">
    <div class="row">
        <div class="col-8 offset-2 mb-5 border p-5 bg-light mt-5 rounded-lg">
            <div class="row">
                <div class="col-6 border-right">
                    <h3 class="mb-3 text-center">Pourquoi creer un compte ?</h3>
                    <div  class="col-10 offset-1">
                        <div class="card">
                            <h5 class="card-header text-center">Bienvenue dans lepticoin</h5>
                            <div class="card-body">
                                <p class="card-text text-white">Some quick example text to build on the card title and make up the bulk of the card's content.
                                nous somme fort blablabla les meilleurs blablabla</p>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-6 border-left">
                    <h3 class='text-center mb-3'>Carte d'identité</h3>
                    <span class="font-weight-bold col-10 offset-1 text-danger">*</span><small class="text-danger"> Format: jpg, png, jpeg, pdf, Taille max: 500 ko</small>
                        <div  class="col-10 offset-1 mt-3 text">
                        <form method="POST" enctype="multipart/form-data">
                                <div class="form-group mb-3">
                                    <label for="idRecto" for="inputGroupFile01"><span class="font-weight-bold text-danger">*</span> Carte d'identité recto</label>
                                    <input type="file" class="form-control-file" name="idRecto" id="idRecto">
                                    
                                </div> 
                                <div class="form-group">
                                    <label for="idVerso" for="inputGroupFile01"><span class="font-weight-bold text-danger">*</span> Carte d'identité verso</label>
                                    <input type="file" class="form-control-file" name="idVerso" id="idVerso">
                                </div>                           
                                <div class="form-group text-center mt-5">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block rounded-pill mb-3">Envoyer</button>
                                    <button type='submit' name="identityIgnore" class="text-danger" style="-webkit-appearance: none; margin:0; padding:0; line-height:100%; border:0px !important; background-color: white;text-decoration: underline;">Ignorer cette étape</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>

<script>
    // Function for dynamique label for upload file name
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
