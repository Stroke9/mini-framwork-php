<html lang="en">
<head>
    <title>My Admin Panel</title>

    
    <!-- Other libs (CDN) -->
    <?php 
        include dirname(__DIR__).'/layout/head.php';
    ?>
    
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="admin.css">

    <!-- Font Awesome JS -->
    <script src="https://kit.fontawesome.com/425ac02770.js" crossorigin="anonymous"></script>

    <!-- jsGrid (for better manipulation table) -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
    
</head>
<body>
    <?php 
        include 'generateDatabase.php';
    ?>
</body>