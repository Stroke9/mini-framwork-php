<?php 
/* -- TO CHANGE : USE CURL INSTEAD --
    include "./sql/getCategory.php";
    include "./common/navBar.php"; 
*/
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h2 class=" mt-3 text-center">Déposer une annonce</h2>
            
            <hr>
        </div>
    </div>
    <div class ="row">
        <div class="col-6 offset-3">
            <div class="card border-primary mt-3 mb-3">
                <div class="card-header text-center">Annonce</div>
                    <div class="card-body text-primary">
                        <form action="./test_back/api.php?action=adCreation" method="post">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Catégories</label>
                                <select class="form-control" name="category" id="exampleFormControlSelect1">
                                    <?php
                                        foreach($categories as $categorie)
                                        {
                                            echo "<option value =".$categorie["id"].">".$categorie['name']."</option>";  
                                        }
                                    ?>
                                </select>
                                </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Titre de l'annonce</label>
                                <input type="text"  name="title" class="form-control" id="formGroupExampleInput" required >
                                </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Description du produit</label>
                                <label for="exampleFormControlTextarea1"></label>
                                <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="3" required ></textarea>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Prix</label>
                                <input type="text" name="price" class="form-control" id="formGroupExampleInput" required >
                                </div>
                                <div class="form-group">
                                <label for="formGroupExampleInput">Localisation</label>
                                <input type="number"  name="zip_code" class="form-control" id="formGroupExampleInput" required >
                                </div>
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile04">
                                            <label class="custom-file-label" for="inputGroupFile04">Image 1</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button">Button</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-10 offset-1">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile04">
                                                <label class="custom-file-label" for="inputGroupFile04">Image 2</label>
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button">Button</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="row mt-3">
                                    <div class="col-10 offset-1">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile04">
                                                <label class="custom-file-label" for="inputGroupFile04">Image 3</label>
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button">Button</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <div class=" mt-3 text-center">
                                <input type="submit" class="btn btn-primary" id="submitValue" value="Envoyé">
                            </div> 
                        </form>
                    </div>   
                    </div>       
                </div>
            </div>   
        </div>