<?php
    
    include "requestCategory.php"; // recupérer les categories dans la base BDD

    //Recupération de ID du ad selectionné pour l'envoyer en value dans l'api
    $id = $_GET['id'];
    
    //Appeler la méthode lookAD et envoyer ID du ad séléctionné
    //INIT cURL
    $curl = curl_init();

    $url=__DIR__.'/../../../../../api/api.php?action=lookAd&value='.$id;
    $url=str_replace('/Applications/MAMP/htdocs','localhost:8888',$url);
    //echo $url; 
  
  //Configuration
  $curl_options=array(
      CURLOPT_URL=>$url,
      CURLOPT_HEADER=>false,
      CURLOPT_RETURNTRANSFER=>true
  );

  curl_setopt_array($curl,$curl_options);

   // Execution
  $myjson=curl_exec($curl); 

   // END (Close)
   curl_close($curl);

   //OUTPUT : 
   //var_dump($myjson);
   $jsonDecoded=json_decode($myjson,true);
   //var_dump($jsonDecoded);
   //echo $jsonDecoded['data'][0]['title'];
?> 

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Création d'annonce</title>
</head>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h2 class=" mt-3 text-center"> L'annonce</h2>
        
                <hr>
            </div>
        </div>
        <div class ="row">
            <div class="col-6 offset-3">
                <div class="card border-primary mt-3 mb-3">
                    <div class="card-header text-center">Annonce</div>
                        <div class="card-body text-primary">
                            <form action="../../../../../api/api.php?action=adUpdate"      method="post">
                                <!-- Un moyen pour faire passer ID customer mais à changer -->
                                <div class="form-group">
                                    <input type="hidden" name="idCustomer" value="<?php echo $jsonDecoded['data'][0]['id'];?>"        
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Catégories</label>
                                    <select class="form-control" name="category" id="exampleFormControlSelect1">
                                        <?php

                                            //j'affiche les catégories dynamiquement
                                            foreach($categories as $categorie)
                                            {
                                                //Affiche en premier la categories selectionné
                                                if($categorie['id'] == $jsonDecoded['data'][0]['id_category_id']){
                                                    echo "<option value =".$categorie["id"]." selected>".$categorie['name']."</option>"; 
                                                }else{
                                                echo "<option value =".$categorie["id"].">".$categorie['name']."</option>";  
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Titre de l'annonce</label>
                                    <input type="text" value='<?php echo $jsonDecoded['data'][0]['title'] ?>' name="title" class="form-control" id="formGroupExampleInput" required >
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Description du produit</label>
                                    <textarea class="form-control" value='<?php echo $jsonDecoded['data'][0]['description'] ?>' name="description" id="exampleFormControlTextarea1" rows="3" required ><?php echo $jsonDecoded['data'][0]['description'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Prix</label>
                                    <input type="text" name="price" class="form-control" value='<?php echo $jsonDecoded['data'][0]['price'] ?>' id="formGroupExampleInput" required >
                                    </div>
                                 <div class="form-group">
                                    <label for="formGroupExampleInput">Localisation</label>
                                    <input type="number" value='<?php echo $jsonDecoded['data'][0]['zip_code'] ?>'  name="zip_code" class="form-control" id="formGroupExampleInput" required >
                                    </div>
                                <div class="row">
                                    <div class="col-10 offset-1">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile04">
                                                <label class="custom-file-label" for="inputGroupFile04">Image 1</label>
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button">Button</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-10 offset-1">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="inputGroupFile04">
                                                    <label class="custom-file-label" for="inputGroupFile04">Image 2</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">Button</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row mt-3">
                                        <div class="col-10 offset-1">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="inputGroupFile04">
                                                    <label class="custom-file-label" for="inputGroupFile04">Image 3</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button">Button</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <div class=" mt-3 text-center">
                                    <input type="submit" class="btn btn-primary" id="submitValue" value="Envoyé">
                                </div> 
                            </form>
                        </div>   
                     </div>       
                 </div>
             </div>   
          </div>
</body>
</html>


