<?php 
require_once $_SESSION['ENV']['root'].'/app/model/curlRequestApi.php';
$_SESSION['LOGIN']['role']='admin';
$_SESSION['LOGIN']['id'] = 81;

$tab=[
    "admin"=>[
        "Information personnel"=>"boardInfoPerso",
        "Annonces en attentes"=>"boardAdAttend",
        "Toutes les annonces"=>"boardAdValider"
    ],
    "customer"=>[
        "Information personnel"=>"boardInfoPerso",
        "Vos Annonces"=>"manageAd",
        "Vos Favoris"=>"favoriteAd"
    ]
];
if(isset($_SESSION['LOGIN']['role'])){
    $role = $_SESSION['LOGIN']['role'];
}
//var_dump($_SESSION);
    
$TABS = $role == 'admin' ? $tab[$role] : 
    $role == 'customer' ? $tab[$role] : null ;

    $sessionLoginId=$_SESSION['LOGIN']['id'];
    /**
     * SET All data for the differents Views used by ROUTE = /boardView
     */
    $adPending=curlRequest('adPending');
    $adPending=$adPending['data']; 

    $adValider=curlRequest('adValider'); 
    $adValider=$adValider['data'];

    $adminInfoPerso = curlRequest("adminInfoPerso",$sessionLoginId);
    $adminInfoPerso = $adminInfoPerso['data'];
    
?>

    <!-- TABS TITLE-->
   
    <?php 
        if(isset($TABS)){
    ?>
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">

    <?php 
            $firstElement = true;
            foreach($TABS as $tabKey=>$tabVal){
                if($firstElement){$activeAttr="active"; $firstElement=false;}else{$activeAttr="";} // To set the active class attribute (or others stuff) only on first element
    ?>
            <a 
                class="nav-item nav-link <?php echo $activeAttr;?>" 
                id="nav-<?php echo $tabVal;?>-tab" 
                data-toggle="tab" 
                href="<?php echo ($firstElement)? "":"#";?>nav-<?php echo $tabVal;?>" 
                role="tab" 
                aria-controls="nav-<?php echo $tabVal;?>" 
                aria-selected="<?php echo ($firstElement) ? "true" : "";?>"
            >
                <?php echo $tabKey?>
            </a>
    <?php 
            } // end -- foreach
    ?>
        </div>
    </nav>
    <!-- END -- TAB TITLES -->


        <!-- TABS CONTENT -->
        <div class="tab-content" id="nav-tabContent">
    <?php
        $firstIndex=true;
        foreach($TABS as $tabIndex => $tabValue){
    ?>
        <div class="tab-pane fade <?php echo ($firstIndex)? "show active":"";?>" 
            id="nav-<?php echo $tabValue;?>" 
            role="tabpanel" 
            aria-labelledby="nav-<?php echo $tabValue;?>-tab"
        >
                <?php include $tabValue.".php";?>
        </div>
    <?php
        $firstIndex=false;
        } // end -- foreach
    ?>
    </div>
<?php
} // end -- if
?>


