<?php
    
    require_once $_SESSION['ENV']['root']."/app/model/curlRequestApi.php"; // recupérer les categories dans la base BDD

    //Recupération de ID du ad selectionné pour l'envoyer en value dans l'api
    $id = isset($_GET['id']) ? $_GET['id'] : 1;
    
    $adValidation=curlRequest('getAdValider',$id);
    var_dump($adValidation);

    $categories=curlRequest('cat');
    var_dump($categories);  
    //echo $adValidation['title'];
?> 

<div class="container-fluid">

    <!--  FORM / EN-TETE -->
    <div class="row">
        <div class="col-12">
            <h2 class=" mt-3 text-center">Annonce <span class="text-success"><?php echo $adValidation['title']; ?></span></h2>
            <hr>
        </div>
    </div>

    <!-- FORM / BODY -->
    <div class ="row">
        <div class="col-6 offset-3">
            <div class="card border-primary mt-3 mb-3">
                <div class="card-header text-center">Annonce</div>
                    <div class="card-body text-primary">
                        <form action="<?php echo $_SESSION['ENV']['root_uri']."/api/controller/api.php?action=updateAd";?>" method="post">

                            <!-- Un moyen pour faire passer ID customer mais à changer -->
                            <div class="form-group">
                                <input type="hidden" name="idCustomer" value="<?php echo $adValidation['id'];?>">        
                            </div>
                            <!-- FORM / CATEGORIES -->
                            <?php require_once 'formCategories.php';?>

                            <!-- FORM / AD - MAIN -->
                            <?php require_once 'formAdMain.php';?>
                            
                            <!-- FORM / IMAGES -->
                            <?php require_once 'formImages.php';?>


                            <div class=" mt-3 text-center">
                                <input type="submit" class="btn btn-primary" id="submitValue" value="Envoyez">
                            </div> 
                        </form>
                    </div>   
                </div>       
            </div>
        </div>   
    </div>



