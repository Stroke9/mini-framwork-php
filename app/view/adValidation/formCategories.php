<div class="form-group">
    <label for="category">Catégories</label>
    <select class="form-control" name="category" id="category">
        <?php
            //j'affiche les catégories dynamiquement
            foreach($categories as $categorie)
            {
                //Affiche en premier la categories selectionné
                if($categorie['id'] == $adValidation['id_category_id']){
                    echo "<option value =".$categorie["id"]." selected>".$categorie['name']."</option>"; 
                }else{
                echo "<option value =".$categorie["id"].">".$categorie['name']."</option>";  
                }
            }
        ?>
    </select>
</div>