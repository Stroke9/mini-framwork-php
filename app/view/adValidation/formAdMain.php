<!-- TITLE -->
<div class="form-group">
    <label for="formGroupExampleInput">Titre de l'annonce</label>
    <input type="text" value='<?php echo $adValidation['title'] ?>' name="title" class="form-control" id="formGroupExampleInput" required >
</div>

<!-- DESC -->
<div class="form-group">
    <label for="exampleFormControlSelect1">Description du produit</label>
    <textarea class="form-control" value='<?php echo $adValidation['description'] ?>' name="description" id="exampleFormControlTextarea1" rows="3" required ><?php echo $adValidation['description'] ?></textarea>
</div>

<!-- PRIX -->
<div class="form-group">
    <label for="formGroupExampleInput">Prix</label>
    <input type="text" 
        name="price" 
        class="form-control" 
        value='<?php echo $adValidation['price'] ?>' 
        id="formGroupExampleInput" 
        required>
</div>


<!-- LOCALISATION -->
<div class="form-group">
    <label for="formGroupExampleInput">Localisation</label>
    <input type="number" 
        value="<?php echo $adValidation['zip_code'];?>"
        name="zip_code" 
        class="form-control" 
        id="formGroupExampleInput" 
        required/>
</div>