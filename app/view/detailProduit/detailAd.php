<?php
//appel de la page curl
include '../app/model/curlRequestApi.php';
$uriParam = isset($_SESSION['ENV']['uri_param'][0]) ? $_SESSION['ENV']['uri_param'][0] : null;
$mycurl = curlRequest("detailAd", $uriParam);
$detail = $mycurl['data'];
$message = $mycurl['message'];
$parent = __DIR__;
$parent = str_replace('/var/www/html', '', $parent);
$rootURL = $_SESSION['ENV']['root_uri'];
?>

<?php
?>
<!-- BREADCRUMB -->
<div class="bg-light">
    <div class="col-8 offset-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $detail['category']['name']; ?></li>
            </ol>
        </nav>

        <!-- CAROUSSEL IMG ANNONCE -->
        <div class="row">
            <div class="col-7">
                <!-- IMAGES -->
                <!-- Shell of our Carousel -->
                <div id="detailCarousel" class="carousel slide mb-3 shadow" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php
                        $countImage = 0;
                        foreach ($detail['images'] as $image) {
                            ?>

                            <li data-target="#detailCarousel" data-slide-to="<?php echo $countImage++; ?>" class="puce-carousel active"></li>
                        <?php }; // end -- foreach
                        ?>
                    </ol>
                    <div class="carousel-inner">
                        <!-- !!!! ajouter class dans css pr thumbnail resize -->
                        <!-- Images in it -->
                        <?php
                        $active = true;
                        foreach ($detail['images'] as $imageArray) { ?>
                            <div class="carousel-item <?php if ($active) {
                                                                echo "active";
                                                                $active = false;
                                                            } ?>">
                                <img class="d-block w-100 img-fluid img-detail-ad img-thumbnail" src="<?php echo $rootURL . '/public/img/examples/upload/' . $imageArray['url']; ?>" alt="Ad slide" width="100%" height="400px">
                            </div>
                        <?php
                        }; // -- end foreach
                        ?>
                    </div>
                    <!-- FLECHES SUR CARROUSSEL -->
                    <a class="carousel-control-prev" href="#detailCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#detailCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!-- // FIN IMAGES  -->

                <!-- ZONE INFOS ANNONCE: -->
                <!-- INFO TITRE -->
                <div class="bg-white shadow-lg px-5 py-3 mb-3 rounded-lg">
                    <div class="row">
                        <div class="col-6">
                            <h5><?php
                                echo $detail['ad']['title'];
                                ?></h5>
                        </div>
                        <!-- INFO PRIX -->
                        <div class="col-6 text-right">
                            <h5><?php
                                echo $detail['ad']['price'];
                                ?> €</h5>
                        </div>
                    </div>
                    <!-- INFO DATE -->
                    <div class="row my-auto">
                        <div class="col-6">
                            <p class="">Postée le:
                                <?php
                                echo $detail['ad']['date'];
                                ?></p>
                        </div>
                        <!-- INFO PARTAGE ANNONCE -->
                        <div class="col-6 text-right">
                            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>"><img width="40px" height="40px" src="<?php echo $rootURL . '/public/img/ico/share.png' ?>"></a>
                            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>"><img width="40px" height="40px" src="<?php echo $rootURL . '/public/img/ico/favori.png' ?>"></a>
                        </div>
                    </div>
                </div>
                <!-- INFO DESCRIPTION -->
                <div class="bg-white shadow-lg p-3 mb-5 rounded-lg">
                    <p>
                        <?php
                        echo $detail['ad']['description'];
                        ?></p>
                </div>
            </div>

            <!-- ZONE INFO VENDEUR -->
            <div class="col-5">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="row">
                            <div class="col-3">
                                <!-- PHOTO PROFIL VENDEUR -->
                                <img src="<?php echo $rootURL . '/public/img/ico/' . $detail['customer']['profil_picture']; ?>" class="mr-5 card-img-top shadow" alt="vendeur" width="100px" height="100px">
                            </div>
                            <div class="col-9 my-auto mx-auto">
                                <div class="row ">
                                    <!-- INFO PSEUDO VENDEUR ET NOTE -->
                                    <a class="px-2" href="<?php echo $_SERVER['REQUEST_URI']; ?>">Vendeur :
                                        <?php
                                        echo $detail['account']['pseudo'];
                                        /* AFFICHE LE NBR D ETOILE SELON LA NOTE */
                                        for ($i = 0; $i < $detail['note']['note']; $i++) {
                                            ?>
                                            <span><img width="20px" height="20px" src="<?php echo $rootURL . '/public/img/ico/star.png' ?>"></span>
                                        <?php
                                        }
                                        ?>
                                    </a><span class="badge badge-secondary text-right">New</span>
                                </div>
                                <!-- INFO CONSULTATION AUTRES ANNONCES DU VENDEUR -->
                                <div class="row">
                                    <a class="p-2" href="<?php echo $_SERVER['REQUEST_URI']; ?>">Consulter mes autres annonces</a>
                                </div>
                            </div>
                        </div>
                        <!-- INFO CONTACT MAIL -->
                        <div class="col-10 offset-1">
                            <div class="row mt-5">
                                <!-- AFFICHE NBR ET MAIL SI CONNECT SINON REDIRIGE USER VERS UNE PAGE CONNEXION-->
                                <a id="sellerMail" class="btn btn-lg btn-white shadow-lg btn-block rounded-pill btn-primary" href="
                                    <?php if (!isset($_SESSION['id'])) {
                                        echo "../logIn/customerConnexion.php"; //a changer, mettre page accueil
                                    }
                                    ?>">Me contacter par Mail</a>
                                <div style="display:none;" id="showMail">
                                    <?php
                                    if (isset($_SESSION['id'])) {
                                        echo $detail['account']['email'];
                                    }
                                    ?>
                                </div>
                            </div>

                            <!-- INFO CONTACT TEL -->
                            <div class="row mt-3">
                                <a id="sellerTel" class="btn btn-lg btn-white shadow-lg btn-block rounded-pill btn-success" href="
                                        <?php if (!isset($_SESSION['id'])) {
                                            echo "../logIn/customerConnexion.php"; /* redirection a modifier */
                                        }
                                        ?>">Me contacter par Téléphone</a>
                                <div style="display:none;" id="showNb">
                                    <?php
                                    if (isset($_SESSION['id'])) {
                                        echo $detail['customer']['phone'];
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <!-- ENCART SUR LE COTE ANNONCES SIMILAIRES -->
                        <h5 class="text-center mt-5">Ces annonces pourraient vous intéresser :</h5>

                        <!-- ANNONCE 1 SUR LE COTE-->
                        <div class="bg-white shadow-lg card mb-3 rounded-lg position-sticky">
                            <a type="button" href="../logIn/customerConnexion.php" class="btn bg-white shadow-lg">
                                <img class="d-block w-100 img-fluid img-thumbnail p-2" src="<?php echo $rootURL . '/public/img/examples/upload/' . $imageArray['url']; ?>" alt="Ad slide" width="100%" height="100px">

                                <div class="card-body shadow-lg">
                                    <h6 class="card-title"><?php
                                                            echo $detail['ad']['title'];
                                                            ?></h6>
                                    <p class="card-text"><?php
                                                            echo $detail['ad']['price'];
                                                            ?> €</p>
                                    <p class="card-text text-muted">Lieu: <?php
                                                                            echo $detail['ad']['zip_code'];
                                                                            ?></p>
                                </div>
                            </a>
                        </div>

                        <!-- ANNONCE 2 SUR LE COTE-->
                        <div class="bg-white shadow-lg card mb-3 rounded-lg position-sticky">
                            <a type="button" href="../logIn/customerConnexion.php" class="btn bg-white shadow-lg">
                                <img class="d-block w-100 img-fluid img-thumbnail p-2" src="<?php echo $rootURL . '/public/img/examples/upload/' . $imageArray['url']; ?>" alt="Ad slide" width="100%" height="100px">

                                <div class="card-body shadow-lg">
                                    <h6 class="card-title"><?php
                                                            echo $detail['ad']['title'];
                                                            ?></h6>
                                    <p class="card-text"><?php
                                                            echo $detail['ad']['price'];
                                                            ?> €</p>
                                    <p class="card-text text-muted">Lieu: <?php
                                                                            echo $detail['ad']['zip_code'];
                                                                            ?></p>
                                </div>
                            </a>
                        </div>
                        <!-- <div class="card">
                            <a href="../logIn/customerConnexion.php" class=" btn bg-white shadow-lg">
                                <div class="card-body shadow-lg">
                                    <h6 class="card-title"><?php
                                                            echo $detail['ad']['title'];
                                                            ?></h6>
                                    <p class="card-text"><?php
                                                            echo $detail['ad']['price'];
                                                            ?> €</p>
                                    <p class="card-text"><small class="text-muted">Lieu: <?php
                                                                                            echo $detail['ad']['zip_code'];
                                                                                            ?> </small></p>
                                </div>
                                <img class="d-block w-100 img-fluid img-thumbnail p-2" src="<?php echo $rootURL . '/public/img/examples/upload/' . $imageArray['url']; ?>" alt="Ad slide" width="100%" height="100px">
                            </a>
                        </div> -->


                    </div>
                </div>
            </div>

            <!-- INFO ICONE CHECK VENDEUR VERIFIE -->
            <!--  <div class="row">
                            <img src="
                            <?php
                            echo $rootUri . '/public/img/ico/usercheck.png';
                            ?>
                            " id="sellerIcon" width="30" height="30">
                        </div> -->
        </div>
    </div>
</div>
<?php

?>

</div>
</div>
</div>
</div>

<!-- <script src="<?php echo $parent . '/detailProduit.js'; ?>"></script> -->
</body>

</html>