<?php
$root = dirname(__DIR__);
$config = parse_ini_file($root.'/config/config.ini');

function curlRequest($action, $value = '', $option = [])
{
    global $root, $config;
    $option = json_encode($option);
    // INIT cURL
    $curl = curl_init();
    $url = $config['api_folder'].$config['api_path'].'?action=' . $action . '&value=' . $value . '&option=' . $option;
    
    $url = str_replace($config['apache_root'], $config['apache_host'], $url);
    $url = str_replace("\\","/",$url); // ! Path in windows are with '\' when cURL only understand fully '/' path 

    // Configuration
    $curl_options = array(
        CURLOPT_URL => $url,
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true
    );

    curl_setopt_array($curl, $curl_options);

    // Execution
    $myjson = curl_exec($curl);
    //var_dump($myjson);

    // END (Close)
    curl_close($curl);

    
    //OUTPUT : 
    $detail = json_decode($myjson, true);

    //var_dump($myjson);
    return $detail;
}
