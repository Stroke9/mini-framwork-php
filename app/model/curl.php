<?php

function curlDynamique($action,$value=''){ 
    
    global $root;
    $curl = curl_init();
    $url=$root.'/api/api.php?action='.$action.'&value='.$value;
  
    $url=str_replace('/Applications/MAMP/htdocs','localhost:8888',$url);
    //echo $url.' ; url';
    // Configuration
    $curl_options=array(
        CURLOPT_URL=>$url,
        CURLOPT_HEADER=>false,
        CURLOPT_RETURNTRANSFER=>true
    );

    curl_setopt_array($curl,$curl_options);
    // Execution
    $myjson=curl_exec($curl); 

    // END (Close)
    curl_close($curl);

    //OUTPUT : 
    //var_dump($myjson);
    $jsonDecoded=json_decode($myjson,true);
    return $jsonDecoded;
};   
?>

