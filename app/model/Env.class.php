<?php 

class Env{

    public function setEnv($param,$val){
        if(isset($_SESSION['ENV'])){
            $_SESSION['ENV'][$param] = $val;
        }else{
            $this->constructEnv();
            $this->setEnv($param,$val);
        }
    }
    
    public function constructEnv(){
        $_SESSION['ENV'] = [];
        return "Done";
    }

    public function getEnv($param){
        return $_SESSION['ENV'][$param];
    }
    
    public function destroyEnv(){
        
        unset($_SESSION['ENV']);
    
    }
    public function destroyEnvVar($var){

        unset($_SESSION['ENV'][$var]);
        
    }
}
?>