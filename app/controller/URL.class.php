<?php 

class URL{
    /**
     * Make variable to get back or next with only button on the page
     * Use an array stocked in SESSION
     * Never stock more than 3 values 
     * When arrived to the limit, use shift (erase first value)
     * Then, reindex the array 
     * And for last, push last value 
     */
    public function navigationHistory($maxSize=3){
        // Initiate our history in SESSION
        if(!isset($_SESSION['history'])){
            $_SESSION['history'] = [];
        }

        // Make a copy of our history into a buffer (to use somme array manipulation methods)
        $bufferShift = $_SESSION['history'];

        // Erase older data to never exceed the max size specified
        $bufferPush=count($bufferShift) > $maxSize ? array_slice($bufferShift,1) : $bufferShift; // array_shift reindex automaticaly the array
        
        // Set the current page / request URI, in the history 
        $lastKey = count($bufferPush) > 0 ? count($bufferPush) - 1 : 0 ;

        if(!isset($bufferPush[$lastKey])){
            array_push($bufferPush,$_SERVER['REQUEST_URI']);
        }else if($bufferPush[$lastKey] != $_SERVER['REQUEST_URI']){
            array_push($bufferPush,$_SERVER['REQUEST_URI']);
        }

        $_SESSION['history']=$bufferPush;
    }

    /**
     * Ways of access to the path specified in URL (with $_SERVER): 
     *  - 'HTTP_REFERER'
     *  - 'REQUEST_URI'
     * @param uri (string) : path info stored in SERVER variable, return (array)
     * @return parsed(array)
     */
    public function parseUri($uri){
        /** 
         * URI can be accessed :
         *  - Delta from 'SCRIPT_NAME' - 'REQUEST_URI' (get all string after the filename in url) 
         * /!\ Method above can be troublesome in case of url Rewriting and others unknown cases ! 
         *  - 'PATH_INFO' (get the URI information from scriptname ; can have same problem as explain above - need to see documentation) 
        */
        $split = explode('/',$uri);

        // Explode method make "" (empty string) in the return array;
        // Can't prevent this, but we can filter the values with a callback function
        // Callback make it easy to specify which values need to be filtered and which not (in this case, only empty strings & null values matter, other false values like 0 need to be keeped !)
        $filtered = array_filter(
            $split, 
            function($element){
                return $element !== '' && !is_null($element);
            }
        );
        // Of course, the indexes of the array are untouched, and so, we need to re-index all of this
        $reindex = array_values($filtered);
        // Already, we only need the first parameters given
        // Need to be changed for others uses !
        return $reindex;
    }
    
}
?>