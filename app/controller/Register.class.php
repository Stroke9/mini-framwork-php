<?php

include "../api/EntityCRUD.class.php";

class Register {

    public $message;
    public $dataRegister;
    public $state;

    public function __construct() {
        $this->state = 1;
    }
    
    public function register1() {

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
    
            $email = $_POST['email'];
            $pseudo = $_POST['pseudo'];
            $emailConfirm = $_POST['emailConfirm'];
            
            $this->message = ["danger" => [], "success" => []];
            $this->dataRegister = ['email' => $email, 'pseudo' => $pseudo];

            foreach ($this->dataRegister as $key => $val) {
                $account = new MyEntity('leptitcoin', 'account');
                $result = $account->get("$key", "WHERE $key = '$val'");
                if($this->dataRegister[$key] == "") {
                    array_push($this->message["danger"], "Le champ " . $key . " doit etre completé !");
                } elseif($result){
                    array_push($this->message["danger"], $key . " deja existant ...");
                }
            }

            if($email !== $emailConfirm) {
                array_push($this->message["danger"], "Les emails ne sont pas identiques");
            }
            
            if(empty($this->message['danger'])) {
                $_SESSION['dataRegister1'] = $this->dataRegister;
                $this->state = 2;
            }
        }
    }

    public function register2() {

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $this->message = ["danger" => [], "success" => []];

            $name = $_POST['name'];
            $fname = $_POST['fname'];
            $password = $_POST['password'];
            $passwordConfirm = $_POST['passwordConfirm'];
            $nb_street = $_POST['nb_street'];
            $street = $_POST['street'];
            $complementary = $_POST['complementary'];
            $zip_code = $_POST['zip_code'];
            $city = $_POST['city'];
            $country = $_POST['country'];

            if(!$complementary) {
                $this->dataRegister = ['nom' => $name, 'prenom' => $fname, 'mot de passe' => $password, 'numero de rue' => $nb_street, 'rue' => $street, 'code postal' => $zip_code, 'ville' => $city, 'pays' => $country];
            } else {
                $this->dataRegister = ['nom' => $name, 'prenom' => $fname, 'mot de passe' => $password, 'numero de rue' => $nb_street, 'rue' => $street, $complementary => 'complement adresse', 'code postal' => $zip_code, 'ville' => $city, 'pays' => $country];
            }

            foreach ($this->dataRegister as $key => $val) {
                if($this->dataRegister[$key] == "") {
                    array_push($this->message["danger"], "Le champ " . $key . " doit etre completé !");
                }
            }

            if($password !== $passwordConfirm) {
                array_push($this->message["danger"], "Les mots de passes ne sont pas identiques !");
            }
            
            if(empty($this->message['danger'])) {
                $_SESSION['dataRegister2'] = $this->dataRegister;
                $_SESSION['register']['state'] = 3;
            }
        }
    }

    public function register3() {

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
      
            $idRecto = $_FILES['idRecto'];
            $idVerso = $_FILES['idVerso'];

            $this->message = ["danger" => [], "success" => []];
            $this->dataRegister = ['ID recto' => $idRecto, 'ID verso' => $idVerso];
        }

        if(isset($_POST['identityIgnore'])) {
            $this->sendToBdd('Non Transmit');
            array_push($this->message['success'], "Votre compte a bien été créer !");
            //header('Refresh: 4; URL=register1.php');
        } else {
            $this->validIdentityDocument();
            $this->sendToBdd('Transmit, en cours de traitement');
            array_push($this->message['success'], "Votre compte a bien été créer !");
            //header('Refresh: 4; URL=register1.php');
            
        }

    }

    function validIdentityDocument() {

        foreach ($this->dataRegister as $key => $val) {
            if($this->dataRegister[$key]['name'] == "") {
                array_push($this->message["danger"], "Le fichier " . $key . " n'a pas été ajouté ou n'a pas été télechargé correctement");
            } elseif ($this->dataRegister[$key]['type'] !== "image/jpg" && $this->dataRegister[$key]['type'] !== "image/jpeg" && $this->dataRegister[$key]['type'] !== "image/png" && $this->dataRegister[$key]['type'] !== "image/pdf"){   
                array_push($this->message["danger"], "La format " . $this->dataRegister[$key]['type'] . " de ". $key . " n'est pas supporté");
            }
            if ($this->dataRegister[$key]['size'] > 500000) {
                array_push($this->message["danger"], "La taille du fichier " . $key . " est superieur a 500 ko");
            }
        }
    }

    function sendToBdd($checkIdentity) {

        if (empty($this->message["danger"])) {
    
            $email = $_SESSION['dataRegister1']['email'];
            $pseudo = $_SESSION['dataRegister1']['pseudo'];
    
            $password = $_SESSION['dataRegister2']['mot de passe'];
            $name = $_SESSION['dataRegister2']['nom'];
            $fname = $_SESSION['dataRegister2']['prenom'];
            $nb_street = $_SESSION['dataRegister2']['numero de rue'];
            $street = $_SESSION['dataRegister2']['rue'];
            $zip_code = $_SESSION['dataRegister2']['code postal'];
            $city = $_SESSION['dataRegister2']['ville'];
            $country = $_SESSION['dataRegister2']['pays'];
    
            if (isset($_SESSION['dataRegister2']['complement adresse'])) {
                $complementary = $_SESSION['dataRegister2']['complement adresse'];
            } else {
                $complementary = '';
            }
    
            $account = new MyEntity('leptitcoin', 'account');
            $checkSend = $account->get("pseudo", "WHERE pseudo = '$pseudo'");

            if (!$checkSend) {
                $account->post("`email`,`pseudo`,`password`,`role`", "'$email', '$pseudo', '$password', 'customer'");
                $idAccount = $account->bdd->bdd->lastInsertId();
        
                $address = new MyEntity('leptitcoin', 'address');
                $address->post("`street`, `street_number`, `zip_code`, `city`, `complementary`, `country`", "'$street', '$nb_street', '$zip_code', '$city', '$complementary', '$country'");
                $idAddress = $address->bdd->bdd->lastInsertId();
        
                $customer = new MyEntity('leptitcoin', 'customer');
                $customer->post("`id_account_id`, `id_adress_id`, `name`, `fname`, `profil_picture`, `rank`, `identity_document`", "'$idAccount', '$idAddress', '$name', '$fname', 'defaultProfil.jpg', 'Non certifié', '$checkIdentity'");
            }
        }
    }
}
