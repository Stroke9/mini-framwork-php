<?php 
/**
 * Act like a Controller, BUT got a lot of method that can be defined in a Model class, rather than here, and be used by extending it or instanciate the object
 */
    class TestRoute{
        
        public $path;
        protected $param;
        protected $option;
        protected $root;
        protected $title;

        public function __construct($method,$param=null,$option=null){
            $this->path = $method;
            $this->param = $param;
            $this->option = $option;
            $this->root = dirname(__DIR__);

            $this->verifyRoute($this->path);
        }

        public function findTitle(){
            $title=[
                    "detailAd"=>"Details Ad",
                    "ad"=>"Ads",
                    "adCreation"=>"Ad Creation",
                    "login"=>"Login",
                    "register"=>"Register",
                    "personalBoard"=>"Personnal Board",
                    "raw"=>"Portal"
            ];
            $this->title = $title[$this->path];
            return $this->title;
        }

        public function verifyRoute($route){
            if(!method_exists($this,$route)){
                //header('Status : 404 Not Found') || "HTTP/1.1 404 Not Found"; || 'Location:'.dirname(__DIR__).'/view/error/404.html' || http_response_code(404);

                header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
                include $this->root.'/view/error/404-page/index.php';
                die();
            }
        }
        public function renderRoute(){
            $method = $this->path;
            $env=new Env();
            if(method_exists($this,$method)){
                if(isset($this->param)){
                    if($this->param !== null)
                        $env->setEnv("uri_param",$this->param);
                        $this->$method($this->param);
                }else{
                    $env->destroyEnvVar('uri_param');
                    $this->$method();
                }
            }
        }
        /**
         * @Route("/detail/{id.ad}", methods={'GET','HEAD'}, name="Details Product")
         */
        public function detailAd($id=null){
            include $this->root.'/view/detailProduit/detailAd.php';
        }
        
        /**
         * @Route("/ad/{?filter}", methods={'HEAD','GET','POST'}, name="Ads")
         */
        public function ad($filter=null){
            include $this->root.'/view/ad/ad.php';
        }

        /**
         * @Route("/adCreation/{data}", methods={'HEAD','POST'}, name="Ad Creation")
         */
        public function adCreation($data=null){
            include $this->root.'/view/adCreation/adCreation.php';
        }
        /**
         * @Route("/adValidation/{id}", methods={'HEAD','GET'}, name="Ad Validation")
         */
        public function adValidation($id=null){
            include $this->root.'/view/adValidation/adValidation.php';
        }

        /**
         * @Route("/login/{credentials}", methods={'POST'}, name="Login")
         */
        public function login($credentials=null){
            include $this->root.'/view/login/login.php';
        }

        /**
         * @Route("/register/{infos}", methods={'POST'}, name="Register")
         */
        public function register($infos=null){
            include $this->root.'/view/register/register.php';
        }

                /**
                     * /!\ FOR TESTING PURPOSE ONLY !
                     *      SAME ROUTE AS ABOVE 
                     */
                public function register2($infos=null){
                    include $this->root.'/view/register/register2.php';
                }

                public function register3($infos=null){
                    include $this->root.'/view/register/register3.php';
                }

        /**
         * @Route("/personalBoard/{account_type}", methods={'POST'}, name="Personnal Board")
         */
        public function personalBoard($account_type=null){
            if($account_type[0] == 'admin'){
                include $this->root.'/view/personalBoard/admin/boardView.php';
            }else if($account_type[0] == 'customer'){
                include $this->root.'/view/personalBoard/customer/boardView.php';
            }
        }
                /**
                 * /!\ FOR TESTING PURPOSE ONLY !
                 *      SAME ROUTE AS ABOVE 
                 */
                public function admin($account_type=null){
                    include $this->root.'/view/personalBoard/admin/board.php';
                }
                public function customer($account_type=null){
                    include $this->root.'/view/personalBoard/customer/board.php';
                }

        /**
         * @Route("/", methods={'GET','HEAD'}, name="raw")
         */
        public function raw(){
            include $this->root.'/view/root/raw.php';
        }

        /**
         * ERROR Routing : 
         * @Route("/?error={code}", methods={'GET', name="error custom")
         */
        public function error(){
            include $this->root.'/view/error/404.html';
        }
    }
?>