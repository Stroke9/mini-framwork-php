<?php 
session_start();

/* 
    // -- A  implémenter (AUTOLOADER) ! -- //
        use App\Controller\Routeur;
        require_once dirname(__DIR__).'/lib/vendor/autoload.php';
        $router = new Routeur();
        $router->test();
*/

// INI ENV Variables 
$root = dirname(__DIR__);
include $root.'/app/model/Env.class.php';

$env = new Env();
$env->setEnv('root',$root);

$rootScript = $_SERVER['SCRIPT_NAME'];
$env->setEnv('root_script',$rootScript);


    // LOAD values from 'config.ini'
    $config = parse_ini_file($root.'/app/config/config.ini');

$replaceRoot = $config['apache_root'];
$replaceHost = $config['apache_host'];

$rootUrl = str_replace($replaceRoot,$replaceHost,$root);
$rootUrl = str_replace("\\","/",$rootUrl);
$env->setEnv('root_url',$rootUrl); // CHANGE THIS TO ONLY PATH AFTER DOMAIN NAME (meaning without 'localhost') ; think for the first '/', before the REAL uri, and directly after localhost

$rootUri = str_replace("localhost","",$rootUrl);
$env->setEnv('root_uri',$rootUri);
//var_dump($_SESSION);


// URL Controller INI
require_once $root.'/app/controller/URL.class.php';
$urlController=new URL();
$urlController->navigationHistory();

    // URI PARSING

    /* -- First Routing method :
    $rawUri = (isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : '/page0';
    $uri = parseUri($rawUri);
    $path = $uri.'.php'; 
    basename($_SERVER['REQUEST_URI']);
    */

    $raw = "/raw";
    if(isset($_SERVER['PATH_INFO'])){
        $rawUri = $_SERVER['PATH_INFO'];
        //var_dump($rawUri);
        $parsedUri = $urlController->parseUri($rawUri);
        //var_dump($parsedUri);
    }
    
    // Route BINDING :
    $route = str_replace('/','',$raw);
    $param = null;

    if(isset($parsedUri)){
        if(count($parsedUri) > 0 && !empty($parsedUri)){
            $route = $parsedUri[0];
            if(count($parsedUri) > 1){
                $param = array_slice($parsedUri,1);
                //var_dump($param);
            }
        }
    }

// ROUTEUR INI
require_once $root.'/app/controller/TestRoute.php';
$routeur = new TestRoute($route, $param);


/**
 * Pertinent element of the URL / URI / URN :
 * REQUEST URI
 * PATH INFO
 * SCRIPT_NAME
 */

 // INI ROUTES for BUTTON -- TESTING ONLY !
 const ROUTES = 
 [
     "detailAd",
     "ad",
     "adCreation",
     "login",
     "register",
     "personalBoard/admin",
     "personalBoard/customer",
     "raw",
     "register2"
 ];


?>

<html lang="en">
<head>
    <!-- LINK to CSS / JS SCRIPT & LIBS + META tags -->
    <?php include $root.'/app/view/layout/head.php'; ?>

    <title><?php if(isset($routeur)) echo $routeur->findTitle(); else echo 'Title';?></title>
</head>
<body>

    <!-- HEADER -->
    <?php include $root.'/app/view/layout/header.php';?>
    
    <!-- VIEW CONTENT -->
    <?php $routeur->renderRoute();?>

    <!-- FOOTER -->
    <?php include $root.'/app/view/layout/footer.php';?>

</body>
</html>